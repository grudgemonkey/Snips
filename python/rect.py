import sys

class Rectangle:
    def __init__(self, base, height):
        self.base = base
        self.height = height
        self.area = base * height

base = int(sys.argv[2])
height = int(sys.argv[3])

if sys.argv[1] == "rectangle":
    rect = Rectangle(base, height)
    print(rect.area)
